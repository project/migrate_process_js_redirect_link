<?php

namespace Drupal\migrate_process_js_redirect_link\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\TransferException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'MigrateProcessJsRedirectLink' migrate process plugin.
 *
 * Example:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: migrate_process_js_redirect_link
 *       source: link
 *     -
 *       plugin: migrate_process_html
 *       jsredirect: false // to be deprecated
 *     -
 *       plugin: dom
 *       method: import
 *     -
 *       plugin: dom_select
 *       selector: //meta[@property="og:image"]/@content
 *     -
 *       plugin: skip_on_empty
 *       method: row
 *       message: 'Field image is missing'
 *     -
 *       plugin: extract
 *       index:
 *         - 0
 *     -
 *       plugin: skip_on_condition
 *       method: row
 *       condition:
 *         plugin: not:matches
 *       regex: /^(https?:\/\/)[\w\d]/i
 *       message: 'We only want a string if it starts with http(s)://[\d\w]'
 *     -
 *       plugin: file_remote_url
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *  id = "migrate_process_js_redirect_link"
 * )
 */
class MigrateProcessJsRedirectLink extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Logger Class object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Returns the HTTP client.
   *
   * @return \GuzzleHttp\ClientInterface
   *   The Guzzle client.
   */
  public function getHttpClient() {
    if (!isset($this->httpClient)) {
      $this->httpClient = \Drupal::httpClient();
    }
    return $this->httpClient;
  }

  /**
   * Constructs a database object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->httpClient = $http_client;
    $this->logger = $logger;
    $configuration += [
      'guzzle_options' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $options = [];
    $http_string_position = strpos($value ?? '', 'http');
    // Check if url is absolute (not relative) and is a url.
    if ($http_string_position === 0 && filter_var($value, FILTER_VALIDATE_URL) !== FALSE) {
      try {
        $response = $this->getHttpClient()->request('GET', $value, $options);
        $data = (string) $response->getBody();
      }
      catch (TransferException $e) {

        // Logs a notice to channel if we get http error response.
        $this->logger->notice('Failed to get (1) URL @url "@error". @code', [
          '@url' => $value,
          '@error' => $e->getMessage(),
          '@code' => $e->getCode(),
        ]);

        return '';
      }
    }
    else {
      // If string does not start with http return empty string.
      return '';
    }

    // Enable JavaScript redirects is enabled by default.
    $jsredirect = TRUE;

    if (filter_var($jsredirect, FILTER_VALIDATE_BOOLEAN)) {
      // Parse into DOMDocument Object.
      $doc = new \DOMDocument();
      // libxml_use_internal_errors is required incase DOMDocument throws an
      // exception when encountering unrecognised html5 tags.
      // https://www.php.net/manual/en/domdocument.loadhtml.php
      // https://stackoverflow.com/questions/1685277/warning-domdocumentloadhtml-htmlparseentityref-expecting-in-entity
      // Set error level.
      $internalErrors = libxml_use_internal_errors(TRUE);
      // Load HTML.
      $doc->loadHTML($data);
      // Restore error level.
      libxml_use_internal_errors($internalErrors);

      // Normally there is a single link so maybe iteration is not required?
      foreach ($doc->getElementsByTagName('a') as $node) {
        $ahref = $node->nodeValue;
      }

      // Check if url is absolute and if so get the HTML from the target page.
      if (strpos($ahref ?? '', 'http') === 0 && filter_var($ahref, FILTER_VALIDATE_URL) !== FALSE) {
        return $ahref;
      }
      else {
        return '';
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('logger.channel.migrate_process_js_redirect_link')
    );
  }

}
