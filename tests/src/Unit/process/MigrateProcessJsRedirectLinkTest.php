<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_process_js_redirect_link\Unit\process;

use Drupal\migrate_process_js_redirect_link\Plugin\migrate\process\MigrateProcessJsRedirectLink;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Tests the migrate_process_js_redirect_link process plugin.
 *
 * @group migrate_process_js_redirect_link
 * @coversDefaultClass \Drupal\migrate_process_js_redirect_link\Plugin\migrate\process\MigrateProcessJsRedirectLinkTest
 */
final class MigrateProcessJsRedirectLinkTest extends MigrateProcessTestCase {

  /**
   * A logger prophecy object.
   *
   * Using ::setTestLogger(), this prophecy will be configured and injected into
   * the container. Using $this->logger->function(args)->shouldHaveBeenCalled()
   * you can assert that the logger was called.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * Mock of http_client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $mockHttp;

  /**
   * The data fetcher plugin definition.
   */
  private array $pluginDefinition = [
    'id' => 'migrate_process_js_redirect_link',
    'title' => 'Migrate Process JS Redirect Link',
  ];

  /**
   * Test MigrateProcessJsRedirectLink with jsredirect redirect.
   */
  public function testMigrateProcessJsRedirectLinkWithJavascriptRedirect() {
    $configuration = [];
    $value = 'https://news.google.com/rss/articles/CBMiVWh0dHBzOi8vd3d3LmxvbmRvbi1maXJlLmdvdi51ay9pbmNpZGVudHMvMjAyMy9qYW51YXJ5L21haXNvbmV0dGUtZmlyZS1zdHJlYXRoYW0taGlsbC_SAQA?oc=5';

    $google = file_get_contents(__DIR__ . '/../../../fixtures/files/google.html');
    $link = 'https://www.london-fire.gov.uk/incidents/2023/january/maisonette-fire-streatham-hill/';
    $mock = new MockHandler([
      new Response(200, [], $google),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $new_link = (new MigrateProcessJsRedirectLink($configuration, 'migrate_process_js_redirect_link', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($link, $new_link, $message = "actual value is not equal to expected");
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();

    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();

    $mockHttp = $this
      ->getMockBuilder('GuzzleHttp\Client')
      ->disableOriginalConstructor()
      ->addMethods(['getBody'])
      ->onlyMethods(['get'])
      ->getMock();
    $this->mockHttp = $mockHttp;

    parent::setUp();

  }

}
