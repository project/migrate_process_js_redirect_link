Migrate Process JS Redirect Link
================
This module provides a Migrate process plugin to enable you to request and 
extract a link that is typically found on Google's RSS Feeds.

## Example 1

```
process:
  title: title
  'field_feed_item_description/format':
    plugin: default_value
    default_value: full_html
  'field_feed_item_description/value': summary
  'field_web_link/uri':
   -
     plugin: migrate_process_js_link
     source: link
  'field_web_link/title': title
```

## Example 2 (With migrate_process_html)

```
process:
  'body/value':
   -
     plugin: migrate_process_js_redirect_link
     source: link
   -
     plugin: migrate_process_html
     jsredirect: false // to be deprecated
   -
     plugin: dom
     method: import
   -
     plugin: dom_select
     selector: //meta[@property="og:image"]/@content
   -
     plugin: skip_on_empty
     method: row
     message: 'Field image is missing'
   -
     plugin: extract
     index:
       - 0
   -
     plugin: skip_on_condition
     method: row
     condition:
       plugin: not:matches
       regex: /^(https?:\/\/)[\w\d]/i
     message: 'We only want a string if it starts with http(s)://[\w\d]'
   -
     plugin: file_remote_url
```



Please note that using `skip_on_condition` with 'matches' requires the excellent
migrate_conditions module. 
https://www.drupal.org/project/migrate_conditions

Author
-----------
* Daniel Lobo (2dareis2do)
